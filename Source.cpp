#include <stdio.h>
#include <omp.h>
#include <stdlib.h>
#include <stdlib.h>
#include <time.h>

#include "bmp_util.h"
#include "stdafx.h"

#define MaxErr 6

int main(int argc, char* argv[]) {
	int IC, IR, TC, TR;
	float* image, *templatee;
	int x1, y1, x2, y2;

	if (argc != 3) {
		printf("Usage: template-matching original.bmp template.bmp \n");
		exit(0);
	}

	image = ReadBMP(argv[1], &IC, &IR);
	templatee = ReadBMP(argv[2], &TC, &TR);

	if (image == 0 || templatee == 0) {
		exit(1);
	}

	if (IC < TC || IR < TR) {
		printf("Error: The template is larger than the picture\n");
		exit(EXIT_FAILURE);
	}

	int count = 0;

	double err = 0;

	for (int ii = 0; ii < IR - TR + 1; ii++) {
		for (int ij = 0; ij < IC - TC + 1; ij++) {
			err = 0;
			for (int ti = 0; ti < TR; ti++) {
				for (int tj = 0; tj < TC; tj++) {
					for (int rgb = 0; rgb < 3; rgb++)
						err = abs((int)(image[(3 * ti*TC + 3 * tj) + (3 * ii*IC + 3 * ij) + rgb] 
							- templatee[3 * ti*TC + 3 * tj + rgb]));

				}
			}

			if (err / (double)(TC*TR * 3) < MaxErr)
				count++;

		}
	}

	printf("count: %d\n", count);

}